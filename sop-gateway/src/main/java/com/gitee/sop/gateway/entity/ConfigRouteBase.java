package com.gitee.sop.gateway.entity;

import lombok.Data;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;


/**
 * 表名：config_route_base
 * 备注：路由配置表
 *
 * @author tanghc
 */
@Table(name = "config_route_base",pk = @Pk(name = "id"))
@Data
public class ConfigRouteBase {
    /**  数据库字段：id */
    private Long id;

    /** 路由id, 数据库字段：route_id */
    private String routeId;

    /** 状态，1：启用，2：禁用, 数据库字段：status */
    private Byte status;
}

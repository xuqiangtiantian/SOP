package com.gitee.sop.gateway.entity;

import lombok.Data;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;
import java.util.Date;


/**
 * 表名：config_gray_instance
 *
 * @author tanghc
 */
@Table(name = "config_gray_instance",pk = @Pk(name = "id"))
@Data
public class ConfigGrayInstance {
    /**  数据库字段：id */
    private Long id;

    /** instance_id, 数据库字段：instance_id */
    private String instanceId;

    /** service_id, 数据库字段：service_id */
    private String serviceId;

    /** 0：禁用，1：启用, 数据库字段：status */
    private Byte status;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;
}

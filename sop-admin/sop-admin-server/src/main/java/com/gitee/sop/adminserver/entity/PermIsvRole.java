package com.gitee.sop.adminserver.entity;

import lombok.Data;

import java.util.Date;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;


/**
 * 表名：perm_isv_role
 * 备注：isv角色
 *
 * @author tanghc
 */
@Table(name = "perm_isv_role",pk = @Pk(name = "id"))
@Data
public class PermIsvRole {
    /**  数据库字段：id */
    private Long id;

    /** isv_info表id, 数据库字段：isv_id */
    private Long isvId;

    /** 角色code, 数据库字段：role_code */
    private String roleCode;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;
}

package com.gitee.sop.adminserver.entity;

import lombok.Data;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;
import java.util.Date;


/**
 * 表名：config_gray
 * 备注：服务灰度配置
 *
 * @author tanghc
 */
@Table(name = "config_gray",pk = @Pk(name = "id"))
@Data
public class ConfigGray {
    /**  数据库字段：id */
    private Long id;

    /**  数据库字段：service_id */
    private String serviceId;

    /** 用户key，多个用引文逗号隔开, 数据库字段：user_key_content */
    private String userKeyContent;

    /** 需要灰度的接口，goods.get1.0=1.2，多个用英文逗号隔开, 数据库字段：name_version_content */
    private String nameVersionContent;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;
}

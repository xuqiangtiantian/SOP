package com.gitee.sop.adminserver.entity;

import lombok.Data;

import java.util.Date;

import com.gitee.fastmybatis.annotation.Pk;
import com.gitee.fastmybatis.annotation.Table;


/**
 * 表名：perm_role
 * 备注：角色表
 *
 * @author tanghc
 */
@Table(name = "perm_role",pk = @Pk(name = "id"))
@Data
public class PermRole {
    /**  数据库字段：id */
    private Long id;

    /** 角色代码, 数据库字段：role_code */
    private String roleCode;

    /** 角色描述, 数据库字段：description */
    private String description;

    /**  数据库字段：gmt_create */
    private Date gmtCreate;

    /**  数据库字段：gmt_modified */
    private Date gmtModified;
}
